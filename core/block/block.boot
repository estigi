<?php
/** @file block.boot
 * Block boot file
 * For the booting process of the module
 */

function block_isityou(){

	global $x, $xx, $user;

	if(user_access('blocks admin', 'block')){
		if($x == 'block/admin'){
			core_load('block', 'admin');
			return block_admin();
		}
	
		if($x == 'block/admin/new'){
			core_load('block', 'admin');
			return block_edit();
		}
	
		if(path_wildcards('block/admin/block/region/%', TRUE)){
			core_load('block', 'admin');
			return block_admin_block($xx[2], $xx[4]);
		}
	
		if(path_wildcards('block/admin/edit/%', TRUE)){
			core_load('block', 'admin');
			return block_edit($xx[3]);
		}

		if(path_wildcards('block/admin/region/%', TRUE) || path_wildcards('block/admin/region/%/%', TRUE)){
			core_load('block', 'admin');
			return block_admin_region($xx[3], $xx[4]);
		}

	}
}
