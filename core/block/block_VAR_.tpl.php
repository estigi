<div class="block block-<?php print $block['module']; ?>" id="block-<?php print $block['module']; ?>-<?php print $block['id']; ?>">
  <div class="blockinner">

    <?php if ($block['title']) { ?><h2 class="title"> <?php print $block['title']; ?> </h2><?php } ?>

    <div class="content">
      <?php print $block['body']; ?>
    </div>

  </div>
</div>
