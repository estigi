<?php
/**
 * @file forms.boot
 * Forms boot file
 * For the booting process of the module
 */

/**
 * Implementation of booting function
 */
function forms_isityou(){

	global $x, $xx, $user;

	switch ($x){

		case 'forms/delete/confirm':
			//Add go_to if user already logged in
			if($user['uid'] == 0){
				path_redirect('user/login');
			}
			else{
				return 'Work in progress';
			}
	}
}
