<?php

/** @file menu.boot
 * Menu boot file
 * For the booting process of the module
 */

function menu_isityou(){

	global $x, $xx, $user;

	if($x == 'menu/admin'){
		core_load('menu', 'admin');
		return menu_admin();
	}

	if(path_wildcards('menu/new', TRUE)){
		core_load('menu', 'admin');
		return menu_new();
	}

	if(path_wildcards('menu/admin/%', TRUE)){
		core_load('menu', 'admin');
		return menu_admin_menu($xx[2]);
	}

	if(path_wildcards('menu/admin/item/%', TRUE)){
		core_load('menu', 'admin');
		return menu_admin_item($xx[3]);
	}

}
