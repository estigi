<?php
/** @file node.boot
 * Boot functions for node module
 *
 */

function node_isityou(){

	global $x, $xx;

	//I need to redo this

	if(path_wildcards('node/edit/%', TRUE)){
		core_load('node', 'edit');
		return node_edit($xx[2]);
	}

	$myx = path_wildcards('node/%');

	if($myx = 'node/%wildcard' && is_numeric($xx[1])){
		return node_view($xx[1]);
	}

	//Convert to a wildcard
	$myx = path_wildcards('node/new/%');

	switch ($myx){
		case 'node':
		return node_view();

		case 'node/new':
			core_load('node', 'edit');
			return node_new_list();

		case 'node/new/%wildcard':
			core_load('node', 'edit');
			return node_new($xx[2]);

		case 'node/new':
			core_load('node', 'edit');
			return node_new();
	}

	//Admin
	if($x == 'node/admin/types'){
		core_load('node', 'admin');
		return node_types_admin();
	}

	//Admin
	if($x == 'node/admin/add/type'){
		core_load('node', 'admin');
		return node_type_new();
	}

	if(path_wildcards('node/admin/type/%', TRUE)){
		core_load('node', 'admin');
		return node_type_edit($xx[3]);
	}

	return FALSE;
}
