<div id="node-<?php print $node['nid']; ?>" class="node<?php if ($node['sticky'] == 1) { print ' sticky'; } ?><?php if ($node['status'] == 0) { print ' node-unpublished'; } ?> clear-block">

<?php print $node['picture_skinned'] ?>

  <h2><a href="<?php print $node['url'] ?>" title="<?php print $node['title'] ?>"><?php print $node['title'] ?></a></h2>

  <div class="meta">
  <?php if ($node['author']): ?>
    <span class="submitted"><?php print 'Submited by ' . $node['author'] . ' on ' . $node['posted'] ?></span>
  <?php endif; ?>

  <?php if ($terms): ?>
    <div class="terms terms-inline"><?php print $terms ?></div>
  <?php endif;?>
  </div>

  <div class="content">
    <?php print $node['body'] ?>
  </div>

  <?php print $node['links']; ?>
</div>
