<?php

function system_isityou(){

	global $x, $xx, $user;
 
	switch ($x){
		case 'system/admin':
			return system_admin_main();
		case 'system/admin/php':
			return system_show_php_info();
		case 'system/admin/settings':
			return system_settings_page('system_settings_form', 'system', array('admin site'), 'System Main Settings');
		case 'system/admin/rebuild/modules':
			return system_rebuild_modules();
			break;
		case 'system/admin/modules':
			return system_modules_overview();
			break;
	}

	if(path_wildcards('system/admin/perms/edit/%', TRUE)){
		if(user_access('admin_perms', 'system')){
			return system_settings_page($xx[4], 'perms');
		}
	}

	if(path_wildcards('system/admin/settings/%', TRUE)){
		if(user_access('admin_modules', 'system')){
			return system_settings_page($xx[3]);
		}
			else{
				return PATH_NO_ACCES;
			}
	}

	if(path_wildcards('system/admin/modules/admin/%', TRUE)){
			return system_modules_admin($xx[4]);
	}

}
