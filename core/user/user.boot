<?php

/**
 * @file user.boot
 * User boot file
 * For the booting process of the module
 *
 * @ingroup User
 */

function user_isityou(){

	global $x, $xx, $user;

	switch ($x){

		case 'user':
			//Add go_to if user already logged in
			if($user['uid'] == 0){
				core_load('user', 'login');
				return user_login();
			}
			else{
				return user_display();
			}
		case 'user/login':
			if($user['uid'] > 0)
				path_redirect('user');

			core_load('user', 'login');
			return user_login();
		case 'user/register':
			if($user['uid'] > 0)
				path_redirect('user');
			core_load('user', 'login');
			return user_register();

		case 'user/edit':
			core_load('user', 'edit');
			return user_edit();
			break;
		case 'user/logout':
			core_load('user', 'login');
			user_logout();
			break;
		case 'user/admin/list':
			core_load('user', 'admin');
			return user_admin_list();
			break;
	}

	if(path_wildcards('user/%', TRUE)){
		return user_display($xx[1]);
	}

	if(path_wildcards('user/edit/%', TRUE)){
		core_load('user', 'edit');
		return user_edit($xx[2]);
	}

	//To edit categories
	if(path_wildcards('user/edit/%/%', TRUE)){
		core_load('user', 'edit');
		return user_edit($xx[2], $xx[3]);
	}

}
