<?php
/**
 * @file user.login
 * User login process
 * Part of the user module, helper functions for the user login/logout process
 *
 * @todo
 *  Use the db functions
 *
 * @ingroup User
 */

/**
 * Logs out a user from the system
 */
function user_logout(){

	global $user;

	user_anonymous();
	session_regenerate();
	path_redirect(core_variable_get('logout_redirect', '<front>', 'user'));

}

/**
 * Creates a very big login form.
 */
function user_login_form_huge(){

	$form['users']['username'] = array(
		'type'  => 'text',
		'text'  => 'User Name or Password',
		'required' => 1
		);

	$form['users']['password'] = array(
		'type'  => 'password',
		'text' =>'Password',
		'required' => 1
		);

	$form['users']['submit'] = array(
		'type' => 'submit',
		'value' => 'Log In',
	);

	return $form;

}

/**
 * Creates a smaller login form.
 */
function user_login_form_tiny(){

	$form['users']['username'] = array(
		'type'  => 'text',
		'class' => 'user login tiny',
		'text'  => 'User Name',
		'required' => 1,
		'inline' => TRUE,
		'size' => 7,
		'end_break' => FALSE
		);

	$form['users']['password'] = array(
		'type'  => 'password',
		'class' => 'user login tiny',
		'text' =>'Password',
		'required' => 1,
		'inline' => TRUE,
		'size' => 7,
		'end_break' => FALSE
		);

	$form['users']['submit'] = array(
		'type' => 'submit',
		'value' => 'Log In',
		'inline' => TRUE,
		'end_break' => FALSE
	);

	return $form;

}

/**
 * Verifies that the user exists and that it is active.
 */
function _user_verify(){

	global $db, $form_status;

	//$user = mysql_fetch_assoc(mysql_query("SELECT users.uid FROM ".$db['pre']."users WHERE (name = '".$_POST['username']."' OR mail = '".$_POST['username']."') AND pass = '".md5($_POST['password'])."' AND status = 1"));

	$user = db_query("SELECT users.uid FROM {PRE_}users WHERE (name = '".$_POST['username']."' OR mail = '".$_POST['username']."') AND pass = '".md5($_POST['password'])."' AND status = 1", TRUE, TRUE);

	if($user != ''){
		return $user['uid'];
	}

	else{
		$form_status = 'with_errors';
		system_warnings('The user does not exist or the password is incorrect or the user is not active.');
		return FALSE;
	}
}

/**
 * Prepares the user for login and session management.
 */
function user_prep_user(){

	global $db, $user;

	db_query("UPDATE ".$db['pre']."users SET access = ".time().", login = ".time()." WHERE uid = ".$user['uid']."");

	session_regenerate();
	path_redirect(core_variable_get('login_redirect', 'system/admin', 'user'));

}

/**
 * Creates a login form acordingly
 */
function user_login_form($style = 'huge'){

	switch($style){
		case 'huge':
			$form = user_login_form_huge();
			return form_form($form, 'post', 'user');
		break;
		case 'tiny':
			$form = user_login_form_tiny();
			return form_form($form, 'post', 'user');
		break;
	}

}

/**
 * Calling function to login a user.
 */
function user_login(){

	global $form_status, $user;

	$form = user_login_form_huge();

	if(isset($_POST['form_id'])){
		$u = _user_verify();
	}

	$content = form_form($form);

	if($form_status == 'verified'){
		//Do the actual user log in and get the roles
		$user = user_load($u, TRUE);
		user_prep_user();
	}

	return $content;

}

/**
 * Creates a user registration form.
 */
function user_register_form(){

	$form['user_register']['username'] = array(
		'type'  => 'text',
		'text'  => 'User Name',
		'help'  => 'Your desired login name',
		'required' => 1
		);

	$form['user_register']['password'] = array(
		'type'  => 'password',
		'text' => 'Password',
		'help' => 'Your access password.',
		'required' => 1
		);

	$form['user_register']['email'] = array(
		'type'  => 'text',
		'text' =>'email',
		'help' => 'An email to confirm your account.',
		'required' => 1
		);

	$form['user_register']['submit'] = array(
		'type' => 'submit',
		'value' => 'Register',
	);

	return $form;

}

/**
 * Helper function for new accounts
 */
function _user_register(&$form){

	$q = "INSERT INTO {PRE_}users (name, pass, mail, created, access, status)
			values('".$form['username']['value']."',
			'".md5($form['password']['value'])."',
			'".$form['email']['value']."',
			".time().",
			1, 1
			)";

	db_query($q);

	if(!$db['error']){
		return TRUE;
	}
		else{ echo 89;
			return FALSE;
		}
}


/**
 * New user registration page
 */
function user_register(){

	global $form_status, $db, $form;

	$form = user_register_form();

	if(isset($_POST['form_id'])){
		$user = db_query("SELECT users.uid FROM {PRE_}users WHERE name = '".$_POST['username']."' OR mail = '".$_POST['email']."'");
		if($db['num_rows'] != 0){
			$form_status = 'with_errors';
			system_warnings('Sorry, but this user name and/or email is already in use' ,'error');
		}
	}

	$content = form_form($form);

	if($form_status == 'verified'){

		include_once './includes/mailroom.inc';

		if(_user_register($form['user_register'])){

			if(!mail_send('New user registration', $form['email'], 'New account')){
				system_warning('No email sent', 'warning');
			}

			system_warnings("Your account has been succesfully created, you may now login to your account", 'advise');
			path_redirect('login');
		}

	}

	return $content;	

}
