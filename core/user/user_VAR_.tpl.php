<div id="user-<?php print $user['uid']; ?>" class="user <?php print $user['roles']?>">

<?php if($user['picture']){ ?>

	<div style="float: right;" id="user_picture" class="user_avatar">
		<?php print $user['picture_skinned']; ?>
	</div>

<?php } ?>

<h1><?php print $user['name'] ?></h1>
Member Since: <?php print date(core_variable_get('time_format', 'Y - M - j \a\t g:i:s a T'), $user['created']) ?> <br />
Last Session: <?php print date(core_variable_get('time_format', 'Y - M - j \a\t g:i:s a T'), $user['login']) ?> <br />

</div>