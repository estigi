<?php

/**
 * @file
 *  Booting system
 * 
 * @ingroup System
 *
 * @todo
 *  Set routines for different system boot options, such as 'without skin'
 */

/**
 * Load the persistent variable table.
 *
 * The variable table is composed of values that have been saved in the table
 * with variable_set() as well as those explicitly specified in the configuration
 * file.
 * @todo
 *   This is more of a wish list: load only the necesary variables on a per case basis
 */
function boot_variable_init($owner = 'system') {

	global $settings;

	$result = db_query("SELECT * FROM {PRE_}variable WHERE owner = '".$owner."'", TRUE);

	if($result){
		foreach($result as $variable){
			$settings['variables'][$variable['owner']][$variable['name']] = unserialize($variable['value']);
		}
	}
}

/**
 * Basic procedures in a page load
 */
function boot_turn_me_on($step = 'ALL_THE_WAY'){
	global $settings, $boot_step, $block_display;

	$settings = array();
	$block_display = 1;

	switch ($step){
		case 'ALL_THE_WAY':
			//Load the core functions
			require_once './includes/core.inc';
			require_once './includes/cache.inc';
			require_once './includes/database.inc';
			require_once './includes/modules.inc';
			require_once './includes/hooks.inc';
			require_once './includes/session.inc';
			require_once './includes/path.inc';

			modules_required_modules(TRUE);

			//We need the configuration file first in order to connect to the databale
			//then we can continue with the rest of the process
			core_conf_init();

			//Connect to the database
			db_set_active();

			//Initiate variables
			boot_variable_init();

			//Start Session
			session_ini();

			//Set Language
			//Right now we are english only, we'll deal with this when the time comes

			//Clear the cache
			cache_clear();

			//Initiate the path
			path_ini_path();

			//Load the skin or theme however you want to call it
			skin_ini();

			//Execute hook boot step ini
			$boot_step = 'ini';
			hooks_invoke('boot');

			break;

		default:
			break;
	}
}

/**
 * Final procedures in a page load
 */
function boot_let_me_down(){

	hooks_invoke('exit');

}

