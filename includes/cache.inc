<?php

/**
 * @file
 *  Cache management
 * 
 * @ingroup System
 */


function cache_clear($cid = NULL, $force = FALSE){

	global $db;

	$rand = rand(0, 10000);

	if($force){
		db_query("DELETE FROM {PRE_}cache");
	}
	elseif($rand > 3000){
		db_query("DELETE FROM {PRE_}cache WHERE expire < ".time()."");
	}
	if(isset($cid)){
		db_query("DELETE FROM {PRE_}cache WHERE cid = '".$cid."'");
	}

}

function cache_set($cid, $data, $expire = -1, $serialize = 1){

	global $db;

	$data = ($serialize == 1 ? serialize($data) : $data);
	$data = str_replace("'", "\\'", $data);
	$expire = ($expire < 0 ? time() + core_variable_get('cache_expire', 10) : $expire);

	//Attempt to update first
	mysql_query("UPDATE ".$db['pre']."cache SET
					data = '".$data."',
					expire = ".$expire.",
					created = ".time().",
					serialized = ".$serialize."
					WHERE cid = '".$cid."'
					");

	if(mysql_affected_rows() < 1){
		mysql_query("INSERT INTO ".$db['pre']." cache (cid, data, expire, created, serialized) values (
			'".$cid."',
			'".$data."',
			".$expire.",
			".time().",
			".$serialize."
			)");

	}

}

function cache_get($id){

	global $db;

	$result = db_query("SELECT * FROM {PRE_}cache WHERE cid = '".$id."'", TRUE, TRUE);

	if($result != ''){
		return unserialize($result['data']);
	}
 
	return FALSE;

}
