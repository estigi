<?php

/**
 * @file
 *  Core and commonly used functions
 * 
 * @ingroup System
 */


/**
 * Variables:
 *   guest_name
 *   cache_expire
 */

/**
 * If you are behind a reverse proxy, we use the X-Forwarded-For header
 * instead of $_SERVER['REMOTE_ADDR'], which would be the IP address
 * of the proxy server, and not the client's.
 *
 * @return
 *   IP address of client machine, adjusted for reverse proxy.
 */
function core_ip_address() {
	global $settings;

	if (!isset($ip_address)) {
		$settings['ip_address'] = $_SERVER['REMOTE_ADDR'];
		if (core_variable_get('reverse_proxy', 0) && array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
			// If an array of known reverse proxy IPs is provided, then trust
			// the XFF header if request really comes from one of them.
			$reverse_proxy_addresses = core_variable_get('reverse_proxy_addresses', array());
			if (!empty($reverse_proxy_addresses) && in_array($ip_address, $reverse_proxy_addresses, TRUE)) {
				// If there are several arguments, we need to check the most
				// recently added one, i.e. the last one.
				$settings['ip_address'] = array_pop(explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']));
			}
		}
	}
	return $settings['ip_address'];
}

/**
 * Validate that a hostname (for example $_SERVER['HTTP_HOST']) is safe.
 *
 * As $_SERVER['HTTP_HOST'] is user input, ensure it only contains characters
 * allowed in hostnames.  See RFC 952 (and RFC 2181). $_SERVER['HTTP_HOST'] is
 * lowercased.
 *
 * @return
 *  TRUE if only containing valid characters, or FALSE otherwise.
 */
function core_valid_http_host($host) {
  return preg_match('/^\[?(?:[a-z0-9-:\]_]+\.?)+$/', $host);
}

 /**
 * The files that we look for are the module files, we then assume that all other files concerning such module
 * are located in the same directory.
 * @todo
 *   Verify if mysql_query is used for some reason, use db_query
 */

function core_get_filename($name, $type = 'module', $reset = FALSE){

	global $db;

	static $files = array();

	if($reset){
		$files = array();
	}

	if(isset($files[$type][$name])){
		return $files[$type][$name];
	}

	$query = mysql_query("SELECT filename FROM".$db['pre']." system WHERE name = '".$name."' AND type = '".$type."'");
	$file = mysql_fetch_assoc($query);

	$files[$type][$name] = $file['filename'];

	return $files[$type][$name];

}

/**
 * Loads files of any kind in the system. If you are not sure of where a module is located or a theme, use this.
 *
 * @bug
 *  The debug section won't return the entire array of loaded files
 */
function core_load($name, $ext = 'module', $type = 'module', $filename = NULL){

	//Locate the file. We use include_once so that it will not be loaded more than once.
	static $loaded_files;

	//Special for debuging
	if($name == 'PRINT_FOR_DEBUG'){
		return $loaded_files;
	}

	if($loaded_files[$type][$name][$ext] == TRUE){
		return TRUE;
	}

	//Used in core modules to avoid extra queries
	if(!isset($filename)){
		$filename = core_get_filename($name, $type);
	}

	if($filename) {
		//For other parts of the module
		if($ext != 'module'){
			$filename = str_replace('.module', '.' . $ext, $filename);
		}

		if(file_exists($filename)){
			$loaded_files[$type][$name][$ext] = TRUE;
			return (include_once "./$filename");
		}
	}

	return FALSE;
}

/**
 * Loads the configuration and sets the base URL, cookie domain, and
 * session name correctly. From the user's settings.php
 * Most of it as seen in Drupal v6.13
 * @todo
 *  Revisit this function, there is a lot to change in order to adjust it to the new system
 */
function core_conf_init() {

	global $settings, $db;

	if(!file_exists('./includes/settings.php') || substr(sprintf('%o', fileperms('./includes/settings.php')), -4) != '0444'){
		echo 'No installation detected. <br> If you already installed your system, you may want to verify the /includes/setings.php file, they must be \'0400\'. <br> You may want to try installing the system by going <a href="install">Here</a>';
		echo '<meta http-equiv="Refresh" content="10; url=install"> <br/>'; 
		echo 'Redirecting... now....';
		exit;
	}
	else{
		include_once './includes/settings.php';
	}

	$settings['conf'] = array();

	if (isset($_SERVER['HTTP_HOST'])) {
		// As HTTP_HOST is user input, ensure it only contains characters allowed
		// in hostnames. See RFC 952 (and RFC 2181).
		// $_SERVER['HTTP_HOST'] is lowercased here per specifications.
		$_SERVER['HTTP_HOST'] = strtolower($_SERVER['HTTP_HOST']);
		if (!core_valid_http_host($_SERVER['HTTP_HOST'])) {
		// HTTP_HOST is invalid, e.g. if containing slashes it may be an attack.
		header('HTTP/1.1 400 Bad Request');
		exit;
		}
	}
	else {
		// Some pre-HTTP/1.1 clients will not send a Host header. Ensure the key is
		// defined for E_ALL compliance.
		$_SERVER['HTTP_HOST'] = '';
	}

	if (isset($base_url)) {
		// Parse fixed base URL from settings.php.
		$settings['base_url'] = $base_url;
		$parts = parse_url($base_url);
		if (!isset($parts['path'])) {
			$parts['path'] = '';
		}
    $settigns['base_path'] = $parts['path'] .'/';
    // Build $base_root (everything until first slash after "scheme://").
    $settings['base_root'] = substr($base_url, 0, strlen($base_url) - strlen($parts['path']));
  }
  else {
    // Create base URL
    $settings['base_root'] = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 'https' : 'http';

    $settings['base_url'] = $base_root .= '://'. $_SERVER['HTTP_HOST'];

    // $_SERVER['SCRIPT_NAME'] can, in contrast to $_SERVER['PHP_SELF'], not
    // be modified by a visitor.
    if ($dir = trim(dirname($_SERVER['SCRIPT_NAME']), '\,/')) {
      $settings['base_path'] = "/$dir";
      $settings['base_url'] .= $base_path;
      $settings['base_path'] .= '/';
    }
    else {
		$settings['base_path'] = '/';
    }
  }

//This name may change
$settings['base_x'] = $settings['base_root'] . $settings['base_url'] . $settings['base_path'];

  if ($cookie_domain) {
		// If the user specifies the cookie domain, also use it for session name.
		$settings['cookie_domain'] = $cookie_domain;
		$settings['session_name'] = $settings['cookie_domain'];
	}
  else {
    // Otherwise use $base_url as session name, without the protocol
    // to use the same session identifiers across http and https.
    list( , $settings['session_name']) = explode('://', $settings['base_url'], 2);
    // We escape the hostname because it can be modified by a visitor.
    if (!empty($_SERVER['HTTP_HOST'])) {
      $settings['cookie_domain'] = text_check_plain($_SERVER['HTTP_HOST']);
    }
  }
  // To prevent session cookies from being hijacked, a user can configure the
  // SSL version of their website to only transfer session cookies via SSL by
  // using PHP's session.cookie_secure setting. The browser will then use two
  // separate session cookies for the HTTPS and HTTP versions of the site. So we
  // must use different session identifiers for HTTPS and HTTP to prevent a
  // cookie collision.
  if (ini_get('session.cookie_secure')) {
    $settings['session_name'] .= 'SSL';
  }
  // Strip leading periods, www., and port numbers from cookie domain.
  $settings['cookie_domain'] = ltrim($settings['cookie_domain'], '.');
  if (strpos($settings['cookie_domain'], 'www.') === 0) {
    $settings['cookie_domain'] = substr($settigns['cookie_domain'], 4);
  }
  $settings['cookie_domain'] = explode(':', $settings['cookie_domain']);
  $settings['cookie_domain'] = '.'. $settings['cookie_domain'][0];
  // Per RFC 2109, cookie domains must contain at least one dot other than the
  // first. For hosts such as 'localhost' or IP Addresses we don't set a cookie domain.
  if (count(explode('.', $settings['cookie_domain'])) > 2 && !is_numeric(str_replace('.', '', $settings['cookie_domain']))) {
   // ini_set('session.cookie_domain', $settings['cookie_domain']);
  }
 // session_name('SESS'. md5($settings['session_name']));
}

function core_variable_get($name, $default, $owner = 'system', $cat = NULL){

	global $settings;

	if($owner != 'system' && !isset($settings[$owner])){
		boot_variable_init($owner);
	}

	return (isset($settings['variables'][$owner][$name]) && $settings['variables'][$owner][$name] != '' ? $settings['variables'][$owner][$name] : $default);

}

/**
 * Set a persistent variable.
 *
 * @param $name
 *   The name of the variable to set.
 * @param $value
 *   The value to set. This can be any PHP data type; these functions take care
 *   of serialization as necessary.
 * @param $owner
 *  The owner of the variable
 *
 */
function core_variable_set($name, $value, $owner) {
	global $db, $settings;
 
	$serialized_value = serialize($value);

	db_query("DELETE FROM {PRE_}variable WHERE name ='".$name."' AND owner ='".$owner."'");

	db_query("INSERT INTO {PRE_}variable (name, value, owner) VALUES ('".$name."', '".$serialized_value."', '".$owner."')");

	$setings['variables'][$owner][$name] = $value;
}

/**
 * Removes a variable(s).
 *
 * @param $name
 *   The name of the variable to set.
 * @param $owner
 *  The owner of this variable(s)
 * @param @all
 *  If you want to remove all variables for this owner
 */
function core_variable_rm($name, $owner, $all = FALSE){

	global $settings;

	if($name){
		db_query("DELETE FROM {PRE_}variable WHERE name = '".$name."' AND owner = '".$owner."'");
		unset($setings['variables'][$owner][$name]);
	}
	//Just for security you must specify the $all parameter
	elseif(!$name && $all){
		db_query("DELETE FROM {PRE_}variable WHERE owner = '".$owner."'");
		unset($setings['variables'][$owner]);
	}

}

/**
 * Determines which module handles the current page request
 * If no 'obvious' module handles this, will ask everybody to see if there is someone who can take care of it
*/
function core_who_handles_this(){

	global $x, $xx, $xxx;

	$modules = modules_list();
	$content = PATH_NOT_FOUND;

	//If there is a module who's name is equals to the first part of the path, we'll call him first

		if(isset($modules[$xx[0]])){
			core_load($xx[0], 'boot');
			if(function_exists($xx[0].'_isityou')){
				if($content = call_user_func($xx[0].'_isityou')){
					return $content;
				}
			}
		}
		//Lets see if someone else handles this
		else{
			foreach($modules as $module => $path){
				core_load($module, 'boot');
				if(function_exists($module.'_isityou')){
					if($content = call_user_func($module.'_isityou')){
						return $content;
					}
				}
			}
		}

	if(!$content || $content == 0){
		$content = PATH_NOT_FOUND;
	}

	return $content;

}
