<?php

/**
 * @file
 *  db_connection
 * 
 * @ingroup System
 */

/**
 * Sets a DB as active
 * @todo
 *  Protect password from the global space
 */
function db_set_active() {

	global $db;

	if (!$db['conn'] = mysql_connect($db['host'], $db['user'], $db['pwd'])) {
			echo 'Could not connect to mysql';
		exit;
	}

	if (!mysql_select_db($db['db'], $db['conn'])) {
		echo 'Could not select database';
		exit;
}

}

/**
 * Unpacks a db query
 */
function db_unpack(&$data){

	foreach($data as $key => $k){
		$var[$key] = $k;
	}

	return $var;
}

/**
 * Sends a db query and replaces tables with their appropriate names
 * 
 * @param $query
 *  The query to be excecuted
 * @param $unpack
 *  If you want the system to unpack the results calling db_unpack
 * @param $single
 *  If you are expecting or want to reinforce to only receive one row
 * @param $pager
 *  For paged queries, not yet implemented
 * @todo
 * Implement pager
 * @bug
 *  mysql_affected_rows and mysql_num_rows are not properly working
 */
function db_query($query, $unpack = FALSE, $single = FALSE, $pager = FALSE){

	global $db;

	static $queries;

	if($query == 'DEBUG'){
		return $queries;
	}

	//Debug
	if(core_variable_get('debug_info', 'No') == 'Yes'){
		if(!isset($queries))
			$queries = 0;
	}

	$queries++;

	//Replace the pre
	$db['query'] = str_replace('{PRE_}', $db['pre'], $query);
	$db['error'] = FALSE;

	$q = mysql_query($db['query'], $db['conn']);

	$db['num_rows'] = @mysql_num_rows($q);

	$db['error'] = mysql_error();

	$db['affected_rows'] = mysql_affected_rows();

	$error_display = core_variable_get('db_errors', 'Database Only');

	if($db['error']){
		if($error_display == 'Screen Only' || $error_display == 'Screen and Database'){
			system_warnings($db['error'], 'error');
			system_warnings($db['query'], 'error');
		}

		return FALSE;

	}
	elseif($db['affected_rows'] > 0){
		if($db['num_rows'] > 0){
			while($item =  mysql_fetch_assoc($q)){
				$results[] = $item;
			}
	
			if($unpack){
				foreach($results as $result){
					$row[] = db_unpack($result);
				}
	
			if(count($row) == 1 && $single){
				$row = $row[0];
			}
	
			return $row;
	
			}
			else
				return $results;
		}
		else{
			//in case of non-select queries
			return TRUE;
		}
	}
	else{
		return FALSE;
	}

}
