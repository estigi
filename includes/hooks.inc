<?php

/**
 * @file
 *  Hook management system
 * 
 * @ingroup System
 */

/**
 * Get a list of modules that implement a hook and viceversa
 * in_you = all modules in a hook
 * in_me  = all hooks in a module
 * in_the_ceiling = everything
 * @todo
 *   Add a pass where a list of hooks may be (re)populated by one module only
 */
function hooks_implements($name, $what = 'in_you'){

	global $db;

	static $modules_hooks;

	//Get a list of all hooks by module if it has never been asked
	if(!$modules_hooks){
		$modules_hooks = array();

		$result = db_query("SELECT hooks, name FROM {PRE_} system WHERE type = 'module' AND hooks != '' AND status = 1", TRUE);

		foreach($result as $hook => $hooks){
			$this_hooks = explode('::', $hooks['hooks']);
			$modules_hooks[$hooks['name']] = $this_hooks;
		}
	}

	//What do you want?	
	//Hooks in you (modules that implement a hook)
	if($what == 'in_you'){
		$modules_in_hook = array();
		$keys = array_keys($modules_hooks);
		foreach($modules_hooks as $this_module => $hooks){
			if(in_array($name, $hooks)){
				$modules_in_hook[] = $this_module;
			}
		}
		return $modules_in_hook;
	}
	//Hooks in me (hooks in a module)
	elseif($what == 'in_me'){
		return $modules_hooks[$module];
	}
	//hooks in the ceiling? (everything)
	else{
		return $modules_hooks;
	}
}

/**
 * Excecutes the hook in each implementing module and skin
 * @todo
 *  Remove the tmp call to the skin and make it part of the regular call
 */
function hooks_invoke($hook){

	global $skin;

	$args = func_get_args();

	//Get a list of all modules that implement this hook
	$modules = hooks_implements($hook);

	foreach($modules as $module){
		core_load($module, 'hooks');
		call_user_func_array($module.'_hooksinyou', $args);
	}

	//Tmp call skin to see if it implements the hook
	if(function_exists($skin['name']. '_hooksinyou')){
		call_user_func_array($skin['name'].'_hooksinyou', $args);
	}
}
