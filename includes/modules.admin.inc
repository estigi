<?php

/**
 * @file
 *  Module administration system
 * 
 * @ingroup System
 */


function modules_install_module($modules){

	foreach($modules as $module){
		core_load($module, 'install');
	}
}

function modules_get_modules_list(){

	//Core Modules
	if ($handle = opendir('./core')) {
		while (false !== ($file = readdir($handle))) {
			if ($file != "." && $file != ".." && is_dir('./core/'.$file)) {
				$list[$file] = './core/'.$file.'/'.$file.'.module';
			}
		}
	closedir($handle);
	}
	//Contributed modules
	if ($handle = opendir('./modules')) {
		while (false !== ($file = readdir($handle))) {
			if ($file != "." && $file != ".." && is_dir('./modules/'.$file)) {
				$list[$file] = './modules/'.$file.'/'.$file.'.module';
			}
		}
	closedir($handle);
	}

	return $list;
}

/**
	Recreates the database table with each modules details.
	@todo
		Delete missing modules
*/
function modules_rebuild_module_settings($list = NULL){

	global $db;

	if(!isset($list)){
		$list = modules_get_modules_list();
		$list_current = modules_list(-1, TRUE);
	}

	foreach($list as $module => $details){
		if(is_file(str_replace('.module', '.ini', $details))){
			$ini = parse_ini_file(str_replace('.module', '.ini', $details), 'sections');
			if(!isset($ini['hooks']['hooks'])){
				$ini['hooks']['hooks'] = array();
			}

			if(array_key_exists($module, $list_current)){
				$q = db_query("UPDATE {PRE_}system SET info = '".serialize($ini['basic_info'])."', hooks = '".implode('::', $ini['hooks']['hooks'])."' WHERE name='".$ini['basic_info']['machine_name']."'");
			}
				else{
					$q = db_query("INSERT INTO {PRE_}system (filename, name, type, status, info, hooks) VALUES ('" . str_replace('./', '', $details) . "', '" . $ini['basic_info']['machine_name'] . "', 'module', 0, '".serialize($ini['basic_info'])."', '".implode('::', $ini['hooks']['hooks'])."') ");
				}

			if(!$db['error']){
				$done[$ini['basic_info']['name']] = 'fine';
			}
			else{
				$done[$ini['basic_info']['name']] = 'bad';
			}
		}
	}

	return $done;

}

function modules_get_module_settings($list){

	foreach($list as $module => $details){
		if(is_file('./'.str_replace('.module', '.ini', $details))){
			$ini[$module] = @parse_ini_file('./'.str_replace('.module', '.ini', $details), 'sections');
		}
	}

	return $ini;

}

function module_parse_details($module){

	$details = '<div id="module_name">'.path_link('system/admin/modules/admin/'.$module['basic_info']['machine_name'], $module['basic_info']['name']).'</div>';
	$details .= '<div id="module_version">v '.$module['basic_info']['version'].'</div>';
	$details .= '<div id="module_details">'.$module['basic_info']['description'].'</div>';
	if(isset($module['dependencies']['depends']))
		$details .= 'Depends on: <div id="module_depends">'.implode(', ', $module['dependencies']['depends']).'</div>';
	if(isset($module['dependencies']['uses']))
	$details .= 'Uses: <div id="module_uses">'.implode(', ', $module['dependencies']['uses']).'</div>';

	return $details;

}

function module_admin_status($status, $name){

	return db_query("UPDATE {PRE_}system SET status = ".$status." WHERE name='".$name."'");

}
