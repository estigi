<?php

/**
 * @file
 *  Module management system
 * 
 * @ingroup System
 */

/**
 * Returns or loads the basic required modules
 *
 * @param $load
 * Load or just return a list of core modules
 *
 */
function modules_required_modules($load = FALSE) {

	global $x, $xx;

	$modules =  array('block', 'text', 'node', 'system', 'user', 'skin', 'form');

	if($load){
		foreach($modules as $required){
			core_load($required,  'module', 'module', 'core/'.$required.'/'.$required.'.module');
		}

	return;
	}

	return $modules;

}

/**
 * Returns a list of modules
 *
 * @param $status
 *  1 (active) 2 (inactive) -1 (both)
 * @param $reset
 *  If you want to regenerate the list
 * 
 */
function modules_list($status = 1, $reset = FALSE){

	static $modules;

	if($status == 1){
		$status = ' AND status = 1';
	}
	elseif($status == 0){
		$status = 'AND status = 0';
	}
	elseif($status == -1){
		$status = '';
	}

	if(!isset($modules) || $reset){

		$list = db_query('SELECT name, filename, status FROM {PRE_}system WHERE type = "module" '.$status.'', TRUE);

		foreach($list as $module){
			$modules[$module['name']] = array($module['name'] => $module['filename'], 'status' => $module['status']);
		}
	}

	return $modules;

}
