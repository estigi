<?php
/**
 * @file
 *  Path functions
 * 
 * @ingroup System
 */

/**
 * Forbidden access to a path
 */
define('PATH_NO_ACCESS', 1);
/**
 * Content not found
 */
define('PATH_NOT_FOUND', 2);

/**
 * Return a path alias or the internal path given an alias
 *
 * @param $path
 *   A path alias.
 * @param $path_language
 *   An optional language code to look up the path in.
 * @param $alias
 *   If what you are giving is an alias
 * @return
 *   The internal path represented by the alias, or the original alias if no
 *   internal path was found.
 */
function path_get_alias($path, $alias = FALSE, $path_language = '') {

	$result = $path;

	if ($src = path_lookup_path($path, $alias, $path_language)) {
		$result = $src['target'];
	}

	return $result;
}

/**
 * Sets paths with wildcards in order to look for their aliases
 *
 * @param $path
 *  The path to be modified
 */
function path_trim($path = NULL){

	global $x;

	//Use the global path if none was provided
	if(!$path){
		$path = $x;
	}

	$path_array = explode("/", $path);

	$count = count($path_array) - 1;

	//Remove the last % in case that there is one
	if($path_array[$count] == "%"){

		$path_array_2 = array_pop($path_array);

		//Lets leave if there is nothing more to look for
		if(count($path_array) == 1){
			return NULL;
		}
	}

	//Lets count again
	$count = count($path_array);

	//And leave if there is nothing else to be done
	if(($count) <= 1){
		return NULL;
	}
 
	$path_array_2 = array_pop($path_array);
	$path = implode("/", $path_array) . '/%';

	return $path;

}

/**
 * Get aliases or paths for an alias
 * @param $path
 *  The path to look for
 * @param $language
 *  The language of the alias Not yet implemented
 * @param $aliases
 *  If this path is an alias, in which case we look for its internal path
 * @todo
 *  Use the wildcard feature when you want to get an alias for a path
 * @bug
 *  It does not work if the path is one word vg: 'user'
 *
 */

function path_lookup_path($path, $alias = FALSE, $language = ''){

	global $settings, $db;

	$path_language = ($language != '' ? $language : $settings['language']);

	$new_path = NULL;
	$orig_path = $path; //A backup path in case a wild card is been used

	//If this is an alias, get the real (usable) path
	if(!$alias){
		//Get the internal path, fallback if no language is set for this path
		while(!$new_path && $path){

			$new_path = db_query("SELECT src AS target FROM {PRE_}url_alias WHERE dst = '".$path."' AND language IN('".$path_language."', '') ORDER BY language DESC", TRUE, TRUE);

			if(!$new_path){
				$path = path_trim($path);
			}

		}
	}
	else{
		while(!$new_path && $path){
		//Get the path alias, fallback if no language is set for this path
			$new_path = db_query("SELECT dst AS target FROM {PRE_}url_alias WHERE src = '".$path."' AND language IN('".$path_language."', '') ORDER BY language DESC", TRUE, TRUE);
 
			if(!$new_path){
				$path = path_trim($path);
			}
		}

	}	

	//If it is a wildcard alias, lets replace it
	if(strpos($path, "/%") !== FALSE){
		//Non wildcard part of the alias
		$a = str_replace("/%", "", $path);
		//Non wildcard part of the new path
		$b = str_replace("/%", "", $new_path['target']);
		//New path with wildcard modified
		$new_path['target'] = str_replace($a, $b, $orig_path);
	}

	return $new_path;
}

/**
 * Initialize the $_GET['q'] variable to the proper normal path.
 */
function path_ini_path(){

	global $x, $xx, $xxx;
	//$x internal path
	//$xx internal path in an array
	//$xxx original request in an array
	//$_GET['x'] = original request by user

	if (!empty($_GET['x'])) {
		$x = path_get_alias(trim($_GET['x'], '/'));
		$xx = explode('/', $x);
		$xxx = explode('/', $_GET['x']);
	}
		else {
			$x = $_GET['x'] = path_get_alias(core_variable_get('site_frontpage', 'node'));
			$xx = $xxx = explode('/', $x);
		}

	//This may be moved somewhere closer to the page loading stage
	if(isset($_GET['r'])){
		path_redirect($_GET['r']);
	}


}

/**
 * Set an internal url depending on the clean url settings
 * @param $path
 *  The path without http:// to set
 */
function path_set_url($path){

	global $settings;

	$new_path = $settings['base_root'] . $settings['base_url'] . $settings['base_path'];
	$path = ($path == '<front>' ? '' : $path); //



	if(core_variable_get('clean_url', FALSE) == 'Yes' ){
		$new_path .= $path;
	}
	elseif($path != ''){
		$new_path .= '?x=' . $path;
	}
	else{
		$new_path .= $path;
	}

	return $new_path;

}

/**
 * Redirects the current page
 *
 * @param $newx
 *  The new address where you want to go
 *  If the path has http:// on it, will assume that it is an external path
 * @param $external
 *  You may just tell the system to assume it is an external path
 */
function path_redirect($newx = NULL, $external = FALSE){

	global $x, $redirect;

	$redirect = TRUE;

	//Determine if it is an external url
	if(strpos($newx, 'http://') !== FALSE){
		$external = TRUE;
	}

	if(!isset($newx)){
		$newx = $x;
	}

	if(!isset($external)){
		$newx = path_set_url(path_get_alias($newx, TRUE));
	}
		else{
			$newx = path_set_url($newx, TRUE);
		}

	boot_let_me_down();

	echo '<meta http-equiv="Refresh" content="0; url='.$newx.'">';
	echo 'Redirecting... if you have problems <a href="'.$newx.'">Click here</a>';
	exit();

}

/**
 * Match a path with wildcards
 * Or convert a path to a wild card
 *
 * @param $path
 *  The path that you want to compare in the form of 'path/to/compare/%' where % is the variable part of the path
 *
 * @param $compare
 *  If you want to compare paths or just convert yours to the path with a wildcard in the possition where your path is different from the current real path
 *    this/is/your/path this/is/their/path would become this/is/%wildcard/path
 * @param $replace
 *  The wildcard that you want to use
 *
 *
 */
function path_wildcards($path, $compare = FALSE, $replace = '%wildcard'){

	global $x, $xx;

	$mypath = explode('/', $path);
	$newpath = '';

	foreach($xx as $elements){

		if(!isset($i)){
			$i = 0;
		}

		if($mypath[$i] == $xx[$i]){
			$newpath .= $xx[$i];
		}
			else{
				$newpath .= $replace;
			}
		$newpath .= '/'; $i++;
	}

	if($compare){
		if(str_replace('%', $replace, $path) == trim($newpath, '/'))
			return TRUE;
		else
			return FALSE;
	}

	return trim($newpath, '/');
}

/**
 * Check if a path matches any pattern in a set of patterns.
 * Originally from Drupal v6.13
 *
 * @param $path
 *   The path to match.
 * @param $patterns
 *   String containing a set of patterns separated by \n, \r or \r\n.
 *
 * @return
 *   Boolean value: TRUE if the path matches a pattern, FALSE otherwise.
 */
function path_match_path($path, $patterns) {
  static $regexps;

	if (!isset($regexps[$patterns])) {
		$regexps[$patterns] = '/^('. preg_replace(array('/(\r\n?|\n)/', '/\\\\\*/', '/(^|\|)\\\\<front\\\\>($|\|)/'), array('|', '.*', '\1'. preg_quote(core_variable_get('site_frontpage', 'node'), '/') .'\2'), preg_quote($patterns, '/')) .')$/';
	}
	return preg_match($regexps[$patterns], $path);
}

/**
 * Creates a formated link
 * @param $url
 *  The destination url
 * @param text
 *  The text to be displayed in the link
 * @param target
 *  The target of the link (_parent, _top, ...)
 * @todo
 *  Add redirection for external urls
 */
function path_link($url, $text, $title = NULL, $target = '_parent'){

	$title = (isset($title) ? $title : $text);

	//Lets see if there is an alias for this path
	$url = path_get_alias($url, TRUE);

	return '<a href="'.path_set_url($url).'" target="'.$target.'" title="'.$title.'">'.$text.'</a>';

}
