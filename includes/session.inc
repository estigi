<?php
/**
 * @file session.inc
 *  Session management
 *  We store sessions in database, this file handles such tasks
 */

function session_open($save_path, $session_name) {
	return TRUE;
}

function session_close() {
  return TRUE;
}

function session_read($key){
	global $user, $db;

	// Write and Close handlers are called after destructing objects since PHP 5.0.5
	// Thus destructors can use sessions but session handler can't use objects.
	// So we are moving session closure before destructing objects.
	//register_shutdown_function('session_write_close');
 
	// Handle the case of first time visitors and clients that don't store cookies (eg. web crawlers).
	if (!isset($_COOKIE[session_name()])){
		user_anonymous();
		return '';
	}

	// Otherwise, if the session is still active, we have a record of the client's session in the database.
	//move this and use the provided function in user.module
	if(!user_sessions($key)){
		$session = isset($user['session']) ? $user['session'] : '';
		user_anonymous($session);
	}

	return $session['user'];

}

function session_write($key, $value) {
	global $user, $db;

	// If saving of session data is disabled or if the client doesn't have a session,
	// and one isn't being created ($value), do nothing. This keeps crawlers out of
	// the session table. This reduces memory and server load, and gives more useful
	// statistics. We can't eliminate anonymous session table rows without breaking
	// the throttle module and the "Who's Online" block.
	if (!session_save_session() || ($user['uid'] == 0 && empty($_COOKIE[session_name()]) && empty($value))) {
		return TRUE;
	}

	mysql_query("UPDATE ".$db['pre']."sessions SET uid = ".$user['uid'].", cache = ".(isset($user['cache']) ? $user['cahe'] : 0).", hostname = '".core_ip_address()."', session = '".$value."', timestamp = ".time()." WHERE sid = '".$key."'");
	if (mysql_affected_rows()) {
		// Last access time is updated no more frequently than once every 180 seconds.
		// This reduces contention in the users table.
		if ($user['uid'] && time() - $user['access'] > core_variable_get('session_write_interval', 180)) {
		mysql_query("UPDATE ".$db['pre']."users SET access = '".time()."' WHERE uid = ".$user['uid']."");
		}
	}
	else {
		// If this query fails, another parallel request probably got here first.
		// In that case, any session data generated in this request is discarded.
		@mysql_query("INSERT INTO ".$db['pre']."sessions (sid, uid, cache, hostname, session, timestamp) VALUES ('".$key."', ".$user['uid'].", ".(isset($user['cache']) ? $user['cache'] : 0).", '".core_ip_address()."', '".$value."', ".time().")");
	}
	
	return TRUE;
}

function session_destroy_sid($sid) {

	global $db;

	mysql_query("DELETE FROM ".$db['pre']."sessions WHERE sid = '".$sid."'");
}

function session_gc($lifetime){

	global $db;

	// Be sure to adjust 'php_value session.gc_maxlifetime' to a large enough
	// value. For example, if you want user sessions to stay in your database
	// for three weeks before deleting them, you need to set gc_maxlifetime
	// to '1814400'. At that value, only after a user doesn't log in after
	// three weeks (1814400 seconds) will his/her session be removed.
	mysql_query("DELETE FROM ".$db['pre']."sessions WHERE timestamp < ".time() - $lifetime."");

	return TRUE;

}

/**
 * Determine whether to save session data of the current request.
 *
 * This function allows the caller to temporarily disable writing of session data,
 * should the request end while performing potentially dangerous operations, such as
 * manipulating the global $user object.  See http://drupal.org/node/218104 for usage
 *
 * @param $status
 *   Disables writing of session data when FALSE, (re-)enables writing when TRUE.
 * @return
 *   FALSE if writing session data has been disabled. Otherwise, TRUE.
 */
function session_save_session($status = NULL) {
  static $save_session = TRUE;
  if (isset($status)) {
    $save_session = $status;
  }
  return ($save_session);
}

/**
 * Called when an anonymous user becomes authenticated or vice-versa.
 */
function session_regenerate() {

	global $db;

	$old_session_id = session_id();
	
	// We code around http://bugs.php.net/bug.php?id=32802 by destroying
	// the session cookie by setting expiration in the past (a negative
	// value).  This issue only arises in PHP versions before 4.4.0
	if (isset($_COOKIE[session_name()])) {
		setcookie(session_name(), '', time() - 42000, '/');
	}
	
	session_regenerate_id();
	
	mysql_query("UPDATE ".$db['pre']."sessions SET sid = '".session_id()."' WHERE sid = '".$old_session_id."'");
}

function session_ini(){

	global $settings, $user;

	session_set_save_handler('session_open', 'session_close', 'session_read', 'session_write', 'session_destroy_sid', 'session_gc');

	session_name('SESS'.md5($settings['session_name']));
	//session_id(md5($settings['session_name'] . $user['uid']));
	session_start();
}

