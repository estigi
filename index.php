<?php

/** @file index.php
 * Index page
 * This page handles all requests and the basic navigation of your website
 */

/**
 * Definition in order to prevent hacking attacks
 */
define('LIFE_IS_GOOD', 1);

global $time;

function microtime_float(){
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}

$time =  microtime_float();

require_once './includes/bootstrap.inc';
boot_turn_me_on();

$content = core_who_handles_this();

//cases are defined in path.inc
switch ($content){
	case '1':
		$content = 'Access denied';
	break;
	case '2':
		$content = '404 Not found?';
	break;
}

//There should be a hook here right before the skin load, specially for the blocks

skin_fini($content);

boot_let_me_down();
