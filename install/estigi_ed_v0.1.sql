-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 24, 2010 at 07:11 PM
-- Server version: 5.1.37
-- PHP Version: 5.2.10-2ubuntu6.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dev_install`
--

-- --------------------------------------------------------

--
-- Table structure for table `blocks`
--

CREATE TABLE IF NOT EXISTS `blocks` (
  `bid` smallint(6) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL,
  `body` longtext,
  `owner` varchar(50) NOT NULL,
  `format` tinyint(4) NOT NULL DEFAULT '1',
  `form` text,
  PRIMARY KEY (`bid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `blocks`
--

INSERT INTO `blocks` (`bid`, `title`, `body`, `owner`, `format`, `form`) VALUES
(2, 'My account', 'global $block_settings;\r\ncore_load(''menu'');\r\necho menu_load(3, $block_settings[''menu_type'']);', 'menu', 3, 'a:1:{s:9:"menu_type";a:6:{s:4:"type";s:6:"select";s:4:"text";s:9:"Menu Type";s:7:"options";a:2:{s:1:"v";s:8:"Vertical";s:1:"h";s:10:"Horizontal";}s:8:"use_keys";b:1;s:5:"value";s:1:"v";s:8:"required";b:1;}}'),
(3, 'Debug Info', 'global $user, $time;\r\necho ''<hr/>'';\r\nprintf("Memory: %s / %s Mb ", number_format(memory_get_usage()/1000000, 2), number_format(memory_get_peak_usage(TRUE)/1000000, 2));\r\necho ''in: '' . round(microtime_float() - $time, 2) . " Seconds";\r\necho " With " . db_query(''DEBUG'') . " queries";\r\n?><pre><?php\r\nprint_r($settings);\r\nprint_r($user);\r\necho $x;\r\necho $xx;\r\necho ''Session name: '' . session_name();echo ''<br>'';\r\necho  ''Session Id: '' . session_id();\r\n$loaded_files = core_load(''PRINT_FOR_DEBUG'');\r\nprint_r($loaded_files);\r\n?></pre>', 'system', 3, NULL),
(13, 'Footer', '<a href="http://estigi.org">esTigi</a> is (will) be a true CMS option specially designed for usability, stability and performance.\r\nCurrently using a modification of the <a href="http://drupal.org/project/bluebreeze">Bluebreeze</a> Theme with the logo of <a href="http://drupal.org/project/tendu">Tendu</a> (Looking for a maintainer by the way)\r\nYou can always get the latest development version with: git clone git://repo.or.cz/estigi.git\r\nor view the public repository at <a href="http://repo.or.cz/w/estigi.git">http://repo.or.cz/w/estigi.git</a>', '1', 1, NULL),
(1, 'esTigi', 'global $block_settings;\r\ncore_load(''menu'');\r\necho menu_load(1, $block_settings[''menu_type'']);', 'menu', 3, '	$tmp_form[''menu_type''] = array(\r\n		''type'' => ''select'',\r\n		''text'' => ''Menu Type'',\r\n		''options'' => array(''v'' => ''Vertical'', ''h'' => ''Horizontal''),\r\n		''use_keys'' => TRUE,\r\n		''value'' => ''v'',\r\n		''required'' => TRUE\r\n	);'),
(19, 'Login', 'echo path_link(''user'', ''Login'', ''Login to your account'');\r\necho ''<br>'';\r\necho path_link(''user/register'', ''Register'', ''Register a new account'');', '1', 3, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `blocks_regions`
--

CREATE TABLE IF NOT EXISTS `blocks_regions` (
  `rid` smallint(6) NOT NULL AUTO_INCREMENT,
  `bid` smallint(6) NOT NULL,
  `region` varchar(50) DEFAULT NULL,
  `skin` varchar(50) DEFAULT NULL,
  `title_ov` varchar(255) DEFAULT NULL,
  `def_view` varchar(4) DEFAULT 'av',
  `roles` varchar(255) DEFAULT NULL,
  `pages_type` tinyint(4) NOT NULL DEFAULT '0',
  `pages` longtext,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `settings` text,
  `pos` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`rid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=61 ;

--
-- Dumping data for table `blocks_regions`
--

INSERT INTO `blocks_regions` (`rid`, `bid`, `region`, `skin`, `title_ov`, `def_view`, `roles`, `pages_type`, `pages`, `status`, `settings`, `pos`) VALUES
(1, 1, 'left', 'bluebreeze', '', '', '', 0, '', 1, '', -10),
(2, 2, 'left', 'bluebreeze', '', '', '3', 0, '', 1, 'a:1:{s:9:"menu_type";s:1:"h";}', 0),
(10, 3, 'bottom', 'bluebreeze', 'Debug Info', '', '3', 0, '', 1, '', 0),
(52, 13, 'footer', 'bluebreeze', '<none>', '', '', 0, '', 1, '', -10),
(59, 19, 'left', 'bluebreeze', '', '', '2', 0, '', 1, '', 0),
(42, 2, 'primary', 'bluebreeze', '<none>', '', '3', 0, '', 1, 'a:1:{s:9:"menu_type";s:1:"h";}', 0);

-- --------------------------------------------------------

--
-- Table structure for table `cache`
--

CREATE TABLE IF NOT EXISTS `cache` (
  `cid` varchar(255) NOT NULL DEFAULT '',
  `data` longblob,
  `expire` int(11) NOT NULL DEFAULT '0',
  `created` int(11) NOT NULL DEFAULT '0',
  `serialized` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`cid`),
  KEY `expire` (`expire`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cache`
--

INSERT INTO `cache` (`cid`, `data`, `expire`, `created`, `serialized`) VALUES
('3e93afc959c26e46bb3c827b7978503d', 0x613a313a7b733a31323a2273797374656d5f61646d696e223b613a393a7b733a393a22736974655f6e616d65223b613a343a7b733a343a2274797065223b733a343a2274657874223b733a343a2274657874223b733a393a2253697465204e616d65223b733a353a2276616c7565223b733a32373a22657354696769202d20444556454c4f504d454e542043454e544552223b733a343a2273697a65223b693a35303b7d733a31313a22736974655f736c6f67616e223b613a343a7b733a343a2274797065223b733a343a2274657874223b733a343a2274657874223b733a363a22536c6f67616e223b733a353a2276616c7565223b733a32323a2254686520434d532076302e31207072652d616c706861223b733a343a2273697a65223b693a35303b7d733a393a22736974655f6d61696c223b613a343a7b733a343a2274797065223b733a343a2274657874223b733a343a2274657874223b733a31333a2244656661756c7420656d61696c223b733a353a2276616c7565223b733a31353a22696e666f4065737469672e6f636f6d223b733a343a2273697a65223b693a35303b7d733a393a22616e6f6e5f6e616d65223b613a343a7b733a343a2274797065223b733a343a2274657874223b733a343a2274657874223b733a31343a22416e6f6e796d6f7573206e616d65223b733a353a2276616c7565223b733a393a22416e6f6e796d6f7573223b733a343a2273697a65223b693a35303b7d733a31343a22736974655f66726f6e7470616765223b613a353a7b733a343a2274797065223b733a343a2274657874223b733a343a2274657874223b733a33313a2246726f6e74207061676520687474703a2f2f6c6f63616c686f73742f692f65223b733a343a2268656c70223b733a32303a224c6561766520626c616e6b20666f72206e6f6465223b733a353a2276616c7565223b733a343a226e6f6465223b733a343a2273697a65223b693a35303b7d733a393a22636c65616e5f75726c223b613a353a7b733a343a2274797065223b733a363a2273656c656374223b733a343a2274657874223b733a31353a2255736520636c65616e207061746873223b733a343a2268656c70223b733a3234333a2241766f696420273f783d2720696e207468652061646472657373206261722e204341524546554c4c2c20796f75206d6179206e6f742062652061626c6520746f2073656520616e797468696e6720696620796f75722073657276657220646f6573206e6f742063757272656e746c7920737570706f7274207468697320666561747572652e20496620796f752072756e20696e746f20616e792074726f75626c65207573652074686973206164647265737320746f207265636f6e66696775726520746869733a20687474703a2f2f6c6f63616c686f73742f692f3f783d73797374656d2f61646d696e2f73657474696e6773223b733a353a2276616c7565223b733a323a224e6f223b733a373a226f7074696f6e73223b613a323a7b693a303b733a323a224e6f223b693a313b733a333a22596573223b7d7d733a31303a2264656275675f696e666f223b613a353a7b733a343a2274797065223b733a363a2273656c656374223b733a343a2274657874223b733a31383a22446973706c617920446562756720696e666f223b733a343a2268656c70223b733a36313a22596f752073686f756c6420616c736f20656e61626c652074686520426c6f636b206173736f746961746564207769746820746869732066656174757265223b733a353a2276616c7565223b733a333a22596573223b733a373a226f7074696f6e73223b613a323a7b693a303b733a323a224e6f223b693a313b733a333a22596573223b7d7d733a393a2264625f6572726f7273223b613a353a7b733a343a2274797065223b733a363a2273656c656374223b733a343a2274657874223b733a32333a22446973706c6179204461746162617365206572726f7273223b733a353a2276616c7565223b733a31313a2253637265656e204f6e6c79223b733a373a226f7074696f6e73223b613a343a7b693a303b733a31313a2253637265656e204f6e6c79223b693a313b733a31393a2253637265656e20616e64204461746162617365223b693a323b733a31333a224461746162617365204f6e6c79223b693a333b733a31343a22446f206e6f7420446973706c6179223b7d733a343a2268656c70223b733a33383a22546869732077696c6c20626520646973706c6179656420666f7220616c6c2061636f756e7473223b7d733a363a227375626d6974223b613a323a7b733a343a2274797065223b733a363a227375626d6974223b733a353a2276616c7565223b733a363a225375626d6974223b7d7d7d, 1272157847, 1272157837, 1);

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE IF NOT EXISTS `menu` (
  `mid` smallint(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `parent` smallint(6) NOT NULL,
  `url` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `expand` tinyint(4) NOT NULL,
  `pos` smallint(6) NOT NULL DEFAULT '0',
  `target` varchar(50) NOT NULL,
  `details` varchar(255) NOT NULL,
  `owner` varchar(50) NOT NULL,
  PRIMARY KEY (`mid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`mid`, `name`, `parent`, `url`, `active`, `expand`, `pos`, `target`, `details`, `owner`) VALUES
(1, 'Main', 0, '', 1, 1, 0, '', '', 'system'),
(2, 'Home', 1, '<front>', 1, 0, -10, '_parent', 'Go back to the home page', 'system'),
(3, 'User Panel', 0, '', 0, 0, 1, '_parent', '', 'user'),
(4, 'My Account', 3, 'user', 1, 0, 0, '_parent', 'View my account', 'user'),
(5, 'Log Out', 3, 'user/logout', 1, 0, 10, '', 'Log out of your account', 'user'),
(6, 'Control Center', 3, 'admin', 1, 1, -8, '_parent', 'Administer my account and/or this website', 'system');

-- --------------------------------------------------------

--
-- Table structure for table `node`
--

CREATE TABLE IF NOT EXISTS `node` (
  `nid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(32) NOT NULL DEFAULT '',
  `language` varchar(12) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL DEFAULT '',
  `uid` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `created` int(11) NOT NULL DEFAULT '0',
  `changed` int(11) NOT NULL DEFAULT '0',
  `comment` int(11) NOT NULL DEFAULT '0',
  `promote` int(11) NOT NULL DEFAULT '0',
  `moderate` int(11) NOT NULL DEFAULT '0',
  `sticky` int(11) NOT NULL DEFAULT '0',
  `tnid` int(10) unsigned NOT NULL DEFAULT '0',
  `translate` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`nid`),
  KEY `node_changed` (`changed`),
  KEY `node_created` (`created`),
  KEY `node_moderate` (`moderate`),
  KEY `node_promote_status` (`promote`,`status`),
  KEY `node_status_type` (`status`,`type`,`nid`),
  KEY `node_title_type` (`title`,`type`(4)),
  KEY `node_type` (`type`(4)),
  KEY `uid` (`uid`),
  KEY `tnid` (`tnid`),
  KEY `translate` (`translate`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `node`
--

INSERT INTO `node` (`nid`, `type`, `language`, `title`, `uid`, `status`, `created`, `changed`, `comment`, `promote`, `moderate`, `sticky`, `tnid`, `translate`) VALUES
(1, 'blog', '', 'First Announcement', 1, 1, 1248722835, 1248722835, 0, 1, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `node_revisions`
--

CREATE TABLE IF NOT EXISTS `node_revisions` (
  `nid` int(10) unsigned NOT NULL DEFAULT '0',
  `uid` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `body` longtext NOT NULL,
  `teaser_full` tinyint(4) NOT NULL DEFAULT '0',
  `log` longtext NOT NULL,
  `timestamp` int(11) NOT NULL DEFAULT '0',
  `format` int(11) NOT NULL DEFAULT '0',
  `current` tinyint(4) NOT NULL DEFAULT '0',
  `revision` int(11) NOT NULL,
  KEY `nid` (`nid`),
  KEY `uid` (`uid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `node_revisions`
--

INSERT INTO `node_revisions` (`nid`, `uid`, `title`, `body`, `teaser_full`, `log`, `timestamp`, `format`, `current`, `revision`) VALUES
(1, 1, 'Welcome', 'Welcome to your new installation of esTigi.\r\n\r\nThis is a first pre-alpha version.\r\n\r\nYou may login using\r\nUser: root\r\nPassword: 123\r\n\r\nNot really much to do now, but many features coming soon....\r\n\r\nRegards.', 0, '', 1248722835, 1, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `node_type`
--

CREATE TABLE IF NOT EXISTS `node_type` (
  `type` varchar(32) NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `module` varchar(255) NOT NULL,
  `description` mediumtext NOT NULL,
  `title_label` varchar(255) NOT NULL DEFAULT '',
  `body_label` varchar(255) NOT NULL DEFAULT '',
  `min_word_count` smallint(5) unsigned NOT NULL,
  `sticky` tinyint(4) NOT NULL DEFAULT '0',
  `published` tinyint(4) NOT NULL DEFAULT '0',
  `promoted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `node_type`
--

INSERT INTO `node_type` (`type`, `name`, `module`, `description`, `title_label`, `body_label`, `min_word_count`, `sticky`, `published`, `promoted`) VALUES
('page', 'Page', 'node', 'this is the description', 'Title', 'Body', 0, 0, 0, 0),
('blog', 'Blog entry', 'blog', 'this is the description', 'Blog Title', 'Blog', 0, 0, 0, 0),
('forum', 'Forum', '', 'This is just a place holder while I develop the real module', 'Title', 'Body', 5, 0, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `permission`
--

CREATE TABLE IF NOT EXISTS `permission` (
  `pid` int(11) NOT NULL AUTO_INCREMENT,
  `rid` int(10) unsigned NOT NULL DEFAULT '0',
  `perm` longtext,
  `tid` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`pid`),
  KEY `rid` (`rid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `permission`
--

INSERT INTO `permission` (`pid`, `rid`, `perm`, `tid`) VALUES
(1, 1, 'access content', 0),
(2, 2, 'access comments, access content, post comments, post comments without approval, post new blog', 0),
(3, 3, 'access comments, access content, post comments, post comments without approval, post new blog', 0);

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE IF NOT EXISTS `role` (
  `rid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`rid`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`rid`, `name`) VALUES
(1, 'Dungeon Master'),
(2, 'Anonymous'),
(3, 'Registered');

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE IF NOT EXISTS `sessions` (
  `uid` int(10) unsigned NOT NULL,
  `sid` varchar(64) NOT NULL DEFAULT '',
  `hostname` varchar(128) NOT NULL DEFAULT '',
  `timestamp` int(11) NOT NULL DEFAULT '0',
  `cache` int(11) NOT NULL DEFAULT '0',
  `session` longtext,
  PRIMARY KEY (`sid`),
  KEY `timestamp` (`timestamp`),
  KEY `uid` (`uid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`uid`, `sid`, `hostname`, `timestamp`, `cache`, `session`) VALUES
(1, '2d813b4836e5742af6fa391b5010bc92', '::1', 1272157837, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `stats`
--

CREATE TABLE IF NOT EXISTS `stats` (
  `nid` int(11) NOT NULL DEFAULT '0',
  `totalcount` bigint(20) unsigned NOT NULL DEFAULT '0',
  `daycount` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`nid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stats`
--


-- --------------------------------------------------------

--
-- Table structure for table `system`
--

CREATE TABLE IF NOT EXISTS `system` (
  `filename` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `type` varchar(255) NOT NULL DEFAULT '',
  `status` int(11) NOT NULL DEFAULT '0',
  `info` text,
  `hooks` longtext,
  PRIMARY KEY (`filename`),
  KEY `modules` (`type`(12),`status`,`filename`),
  KEY `bootstrap` (`type`(12),`status`,`filename`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `system`
--

INSERT INTO `system` (`filename`, `name`, `type`, `status`, `info`, `hooks`) VALUES
('core/menu/menu.module', 'menu', 'module', 1, 'a:5:{s:4:"name";s:4:"Menu";s:7:"version";s:3:"0.1";s:12:"machine_name";s:4:"menu";s:11:"description";s:16:"Core Menu module";s:5:"group";s:4:"core";}', 'admin_page'),
('core/system/system.module', 'system', 'module', 1, 'a:5:{s:4:"name";s:13:"esTigi System";s:7:"version";s:3:"0.1";s:12:"machine_name";s:6:"system";s:11:"description";s:50:"Basic esTigi system with administration functions.";s:5:"group";s:4:"core";}', 'boot::exit'),
('core/block/block.module', 'block', 'module', 1, 'a:6:{s:4:"name";s:5:"Block";s:7:"version";s:3:"0.1";s:12:"machine_name";s:5:"block";s:11:"description";s:19:"Core Blocks module.";s:5:"group";s:4:"core";s:7:"website";s:25:"http://localhost/dev/core";}', 'boot::admin_page'),
('core/text/text.module', 'text', 'module', 1, 'a:5:{s:4:"name";s:4:"Text";s:7:"version";s:3:"0.1";s:12:"machine_name";s:4:"text";s:11:"description";s:40:"Core text module with filtering features";s:5:"group";s:4:"core";}', ''),
('core/node/node.module', 'node', 'module', 1, 'a:5:{s:4:"name";s:4:"Node";s:7:"version";s:3:"0.1";s:12:"machine_name";s:4:"node";s:11:"description";s:17:"Core Node module.";s:5:"group";s:4:"core";}', 'links::admin_page'),
('core/skin/skin.module', 'skin', 'module', 1, 'a:5:{s:4:"name";s:4:"Skin";s:7:"version";s:3:"0.1";s:12:"machine_name";s:4:"skin";s:11:"description";s:48:"Core Skin module, this makes your site look good";s:5:"group";s:4:"core";}', ''),
('core/user/user.module', 'user', 'module', 1, 'a:5:{s:4:"name";s:4:"User";s:7:"version";s:3:"0.1";s:12:"machine_name";s:4:"user";s:11:"description";s:44:"Core user module with profile implementation";s:5:"group";s:4:"core";}', 'admin_page::node_display'),
('skins/bluebreeze/bluebreeze.module', 'bluebreeze', 'theme', 1, NULL, NULL),
('core/form/form.module', 'form', 'module', 1, 'a:5:{s:4:"name";s:4:"Form";s:7:"version";s:3:"0.1";s:12:"machine_name";s:4:"form";s:11:"description";s:16:"Core Form module";s:5:"group";s:4:"core";}', '');

-- --------------------------------------------------------

--
-- Table structure for table `url_alias`
--

CREATE TABLE IF NOT EXISTS `url_alias` (
  `pid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `src` varchar(128) NOT NULL DEFAULT '',
  `dst` varchar(128) NOT NULL DEFAULT '',
  `language` varchar(12) NOT NULL DEFAULT '',
  PRIMARY KEY (`pid`),
  UNIQUE KEY `dst_language` (`dst`,`language`),
  KEY `src_language` (`src`,`language`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `url_alias`
--

INSERT INTO `url_alias` (`pid`, `src`, `dst`, `language`) VALUES
(1, 'node/rss.xml', 'rss.xml', ''),
(2, 'system/admin', 'admin', ''),
(3, 'user/register', 'register', ''),
(4, 'user/login', 'login', ''),
(5, 'node/new/%', 'new/%', '');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL DEFAULT '',
  `pass` varchar(32) NOT NULL DEFAULT '',
  `mail` varchar(64) NOT NULL DEFAULT '@',
  `theme` varchar(255) DEFAULT NULL,
  `signature` tinytext,
  `signature_format` smallint(6) DEFAULT '1',
  `created` int(11) NOT NULL DEFAULT '0',
  `access` int(11) NOT NULL DEFAULT '0',
  `login` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `timezone` varchar(8) DEFAULT NULL,
  `language` varchar(12) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `data` longtext,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `name` (`name`),
  KEY `access` (`access`),
  KEY `created` (`created`),
  KEY `mail` (`mail`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`uid`, `name`, `pass`, `mail`, `theme`, `signature`, `signature_format`, `created`, `access`, `login`, `status`, `timezone`, `language`, `picture`, `data`) VALUES
(0, 'estigi', '', '', '', '', 0, 0, 1249759237, 1249759237, 0, NULL, '', '', NULL),
(1, 'root', '202cb962ac59075b964b07152d234b70', 'root@estigi.org', '', 'Be the change that you want to see in the world\r\n--\r\nMahatma Gandhi', 1, 1248672765, 1272157681, 1272157681, 1, '-21600', '', 'files/def/avatars/uid_1.jpg', 'a:4:{s:18:"admin_compact_mode";b:1;s:13:"form_build_id";s:37:"form-50179f2ecfda5fc54a3e5f13690a5d3f";s:14:"picture_delete";s:0:"";s:14:"picture_upload";s:0:"";}');

-- --------------------------------------------------------

--
-- Table structure for table `users_roles`
--

CREATE TABLE IF NOT EXISTS `users_roles` (
  `uid` int(10) unsigned NOT NULL DEFAULT '0',
  `rid` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`,`rid`),
  KEY `rid` (`rid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_roles`
--

INSERT INTO `users_roles` (`uid`, `rid`) VALUES
(1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `variable`
--

CREATE TABLE IF NOT EXISTS `variable` (
  `name` varchar(128) NOT NULL DEFAULT '',
  `value` longtext NOT NULL,
  `owner` varchar(255) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `variable`
--

INSERT INTO `variable` (`name`, `value`, `owner`) VALUES
('site_slogan', 's:22:"The CMS v0.1 pre-alpha";', 'system'),
('site_name', 's:27:"esTigi - DEVELOPMENT CENTER";', 'system'),
('site_mail', 's:15:"info@estig.ocom";', 'system'),
('front_page', 'i:5;', 'node'),
('db_errors', 's:11:"Screen Only";', 'system'),
('debug_info', 's:3:"Yes";', 'system'),
('anon_name', 's:9:"Anonymous";', 'system'),
('display_logo', 'i:1;', 'bluebreeze'),
('logo', 's:8:"logo.png";', 'bluebreeze'),
('site_frontpage', 's:4:"node";', 'system'),
('user_register', 'N;', 'user'),
('user_login', 's:7:"<front>";', 'user'),
('user_logout', 's:0:"";', 'user');
