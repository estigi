<?php

define('LIFE_IS_GOOD', 'YES');

//Errors
define('ERROR', 0);
define('ERROR_DB', 1);
define('ERROR_SETTINGS', 2);

$fine = -1;

$error = FALSE;

if(file_exists('../includes/settings.php')){
	echo 'The system has already been installed.';
	exit;
}

$dir = dirname($_SERVER['SCRIPT_FILENAME']);
$url = "http://" . $_SERVER['HTTP_HOST'] . str_replace("install", "", dirname($_SERVER ['REQUEST_URI']));

function setup_db_script(){

	$new_file = './estigi_tmp.sql';

	$file = file("./estigi_ed_v0.1.sql");

	$file = implode('', $file);

	$replace = array('`blocks`', '`blocks_regions`', '`cache`', '`menu`', '`node`', '`node_revisions`', '`node_type`', '`permission`', '`role`', '`sessions`', '`stats`', '`system`', '`url_alias`', '`users`', '`users_roles`', '`variable`');

	foreach($replace as $t => $key){
		$file = str_replace($key, "`".$_POST['db_pre'].str_replace("`", "" , $key)."`", $file);
	}

	$e = @file_put_contents($new_file, $file);
	$mod_f = @chmod($new_file, 0777);

	return $e;
	
}

function install_db(){

	if(setup_db_script()){
		system("mysql -u ".$_POST['db_user']." --password=".$_POST['db_password']." ".$_POST['db_name']." < ".dirname($_SERVER['SCRIPT_FILENAME'])."/estigi_tmp.sql", $here);
		return $here;
	}

	return FALSE;

}

function db(){

	if (@!$db = mysql_connect($_POST['db_server'], $_POST['db_user'], $_POST['db_password'])) {
			//echo 'Could not connect to mysql' . mysql_error() . try_again();
		return FALSE;
	}

	if (@!mysql_select_db($_POST['db_name'], $db)) {
		//echo 'Could not select database' . try_again();
		return FALSE;
		//exit;
	}

	return TRUE;
}

function create_settings(){

	$new_file = '../includes/settings.php';

	$file = file("./settings.default");

	$file = implode('', $file);

	$replace = array("\$db['user'] = '';" => "\$db['user'] = '".$_POST['db_user']."';",
						  "\$db['pwd']  = '';" => "\$db['pwd'] = '".$_POST['db_password']."';",
						  "\$db['db']   = '';" => "\$db['db'] = '".$_POST['db_name']."';",
						  "\$db['host'] = 'localhost';" => "\$db['host'] = '".$_POST['db_server']."';"); //,
						  //"\$db['pre']  = '';" => "\$db['pre'] = '".$_POST['db_pre']."';");

	foreach($replace as $t => $key){

		$file = str_replace($t, $key, $file);

	}

	$e = @file_put_contents($new_file, $file);
	$mod_f = @chmod($new_file, 0444);
	$mod_d = @chmod('../includes', 0444);

	return $e;

}

function install_me(){

	if(!db()){
		$e = mysql_error();
		return "I could not connect to your database. <br />This is the error I got: " . $e . " <br />";
	}

	if(install_db() != 0){
		return "I could not populate your database. <br />";
	}

	if(!create_settings()){
		return "I could not create the settings.php file. No idea why. <br />";
	}

	return FALSE;
}

if(isset($_POST['install_estigi'])){

	$installing = TRUE;

	$error = install_me();

}
	else{
			$installing = FALSE;
	}

function verify_requirements($error){

	if(!is_writable('../includes')){
		$error .= 'Your \'includes\' directory is not writable by this script, you must set its permissions acordingly. <br>';
	}

	if(!is_writable('../install')){
		$error .= 'Your \'install\' directory is not writable by this script, you must set its permissions acordingly. <br>';
	}

	return $error;
}

?>

<html>
<head>
<link type="text/css" rel="stylesheet" media="all" href="style.css" />
<title>esTigi installation form</title>
</head>
<body>

<br /><br />
<div align="center">

<?php if($installing && !$error){ ?>
	<div class="fine">

		<?php $url = $_SERVER['HTTP_HOST'] . str_replace("install", "", dirname($_SERVER ['REQUEST_URI'])); ?>

		Setup process is complete (I hope). <br >
		Please go to your new website and start having fun. <br >
		Your new site: <a href="http://<?php print $url; ?>">Here</a> <br />
		<meta http-equiv="Refresh" content="3; url=http://<?php echo $url; ?>"> <br />
		Redirecting... now....
	</div>
	<br >
	<br >
<?php } ?>

<?php

	$error = verify_requirements($error);

	if($error){
		echo '<div class="warnings">' . $error;
		echo 'Correct this problems and <a href="index.php">try again</a><br />';
		echo '</div>';
		exit;
	}

?>
<h1>Welcome to esTigi</h1>
Fill in this form with your installation details, and click on 'Install'. <br/>
If everything goes well, you will have a fully installed system very soon.

<br><br>But WARNING!!!!<br /> This is a pre-beta install script, I strongly recommend using this intead: <a href="README">README</a> <br><br><br>

<div style="float: left;">
		
</div>

<table>
	<tr>
		<td>
			<img src="logo.jpg" width="250"/>
		</td>
		<td>
<form name="install_form" action="index.php" method="post">
<table class="install_table">
	<tr>
		<th colspan="2">Database Settings
		</th>
	</tr>
	<tr>
		<td>
			Database Name
		</td>
		<td><input type="text" value="" name="db_name"> <br> (Must exist)
		</td>
	</tr>
	<tr>
		<td>Database Prefix
		</td>
		<td><input type="text" value="(Not in use at the moment)" name="db_pre" disabled>
		</td>
	</tr>
	<tr>
		<td>Database User Name
		</td>
		<td><input type="text" value="" name="db_user">
		</td>
	</tr>
	<tr>
		<td>Database Password
		</td>
		<td><input type="password" value="" name="db_password">
		</td>
	</tr>
	<tr>
		<td>Database Server
		</td>
		<td>
			<input type="text" value="localhost" name="db_server">
		</td>
	</tr>
	<tr>
		<td align="center" colspan="2">
			<input type="submit" name="install_estigi" value="Install">
		</td>
	</tr>
</table>

</form>
		</td>
	<tr>
</table>

</div>

</body>
</html>