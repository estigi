<div class="node<?php if ($sticky) { print " sticky"; } ?><?php if (!$status) { print " node-unpublished"; } ?>" id="node-<?php print $node->nid; ?>">
  <?php if ($page == 0): ?>
    <h2 class="title">
      <a href="<?php print $node_url ?>"><?php print $title ?></a>
    </h2>
  <?php endif; ?>

  <?php if ($node['picture_skinned']) print $node['picture_skinned'] ?>  
  
  <?php if ($submitted || !empty($terms)): ?>
  <div class="meta<?php if (!empty($terms)) : ?> with-taxonomy<?php endif; ?>">
  
    <?php if ($submitted): ?>
      <div class="submitted"><?php print t('Posted !date by !name', array('!date' => format_date($node->created, 'custom', "F jS, Y"), '!name' => theme('username', $node))); ?></div> 
    <?php endif; ?>
    
    <?php if (!empty($terms)) : ?> 
      <div class="taxonomy"><?php print $terms ?></div>
    <?php endif; ?>
    
  </div>
  <?php endif; ?>
  
  <div class="content">
    <?php print $content?>
  </div>
  
  <?php if ($links): ?>
    <div class="links">
      <?php print $links ?>
    </div>
  <?php endif; ?>
  
</div>
