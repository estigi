<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language ?>" xml:lang="<?php print $language->language ?>">

<head>
<title><?php print $skin['head_title'] ?></title>
<?php print $skin['head'] ?>
<?php print $skin['styles'] ?>
<?php print $skin['scripts'] ?>
<script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE - Gotta love IE */ ?> </script>
</head>

<body id="<?php if ($is_front) { print 'home'; } else { print 'second'; } ?>">
<div id="page" class="<?php if ($skin['regions']['left'] != '' || $skin['regions']['right'] != '') { print "one-sidebar"; } if ($skin['regions']['right'] && $skin['regions']['left']) { print " two-sidebars"; }?>">

	<div id="header">
	
		<div id="logo-title">
		
		<?php if ($skin['logo']): ?>
			<a href="<?php print $skin['site_path'] ?>" title="<?php print 'Home' ?>">
				<img src="<?php print $skin['logo'] ?>" alt="<?php print 'Home' ?>" id="logo" />
			</a>
		<?php endif; ?>
		
		<?php if ($skin['site_name']): ?>
			<h1 id='site-name'>
				<a href="<?php print $skin['site_path'] ?>" title="<?php print 'Home' ?>">
				<?php print $skin['site_name'] ?>
				</a>
			</h1>
		<?php endif; ?>

		<?php if ($skin['site_slogan']): ?> 
			<div id='site-slogan'>
				<?php print $skin['site_slogan'] ?>
			</div>
		<?php endif; ?>
		
		</div>
		
		<div class="menu <?php print isset($skin['regions']['primary']) ? 'withprimary' : ''; print isset($skin['regions']['secondary']) ? 'withsecondary' : ''?>">
			<?php if (isset($skin['regions']['primary'])): ?>
				<div id="primary" class="clear-block">
				<?php print $skin['regions']['primary'] ?>
				</div>
			<?php endif; ?>
			
			<?php if (isset($secondary_links)): ?>
				<div id="secondary" class="clear-block">
			<?php print theme('links', $secondary_links, array('class' => 'links secondary-links')) ?>
				</div>
			<?php endif; ?>
		</div>
		
		<?php if ($skin['regions']['header']): ?>
		<div id="header-region">
			<?php print $skin['regions']['header'] ?>
		</div>
		<?php endif; ?>
		
	</div>

	<div id="container" class="<?php if ($skin['regions']['left'] != '') { print "withleft"; } if ($skin['regions']['right']) { print " withright"; }?> clear-block">
		
	<div id="main-wrapper">
	<div id="main" class="clear-block">
		<?php if ($skin['regions']['content_top']):?><div id="content-top"><?php print $skin['regions']['content_top'] ?></div><?php endif; ?>
		<?php if ($skin['page_title']): ?><h1 class="title"><?php print $skin['page_title'] ?></h1><?php endif; ?>
		<?php if ($skin['tabs']): ?><div class="tabs"><?php print $skin['tabs'] ?></div><?php endif; ?>
		<?php print $help ?>
		<?php print $skin['messages'] ?>
		<?php print $content ?>
		<?php if ($skin['regions']['content_bottom']): ?><div id="content-bottom"><?php print $skin['regions']['content_bottom'] ?></div><?php endif; ?>
	</div>
	</div>

		<?php if ($skin['regions']['left']): ?>
		<div id="sidebar-left" class="sidebar">
			<?php print $skin['regions']['left'] ?>
		</div>
		<?php endif; ?>

		<?php if ($skin['regions']['right']): ?>
		<div id="sidebar-right" class="sidebar">
			<?php print $skin['regions']['right'] ?>
		</div>
		<?php endif; ?>

	</div>

	<div id="footer">
		<div id="footer-break">&nbsp;</div>
		<?php print $skin['regions']['footer'] ?>
	</div>

		<?php if ($skin['regions']['bottom']): ?>
		<div id="bottom" class="bottom">
			<?php print $skin['regions']['bottom']; ?>
		</div>
		<?php endif; ?>

	<?php print $closure //No idea what this is, so it may go away soon ?>

</div>

</body>
</html>
